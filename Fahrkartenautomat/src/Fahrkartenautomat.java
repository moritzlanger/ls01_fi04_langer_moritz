﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		String wiederholen;

		do {

			zuZahlenderBetrag = farkartenBestellungErfassen();

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nMöchten Sie einen weitere Tickets kaufen [Ja][Nein]");

			do {

				wiederholen = tastatur.next();

				if (!wiederholen.equals("Ja") && !wiederholen.equals("ja") && !wiederholen.equals("JA")
						&& !wiederholen.equals("Nein") && !wiederholen.equals("nein") && !wiederholen.equals("NEIN")) {
					System.out.println("Ungültige Eingabe - Geben Sie nur [Ja][Nein] ein");
				}

			} while (!wiederholen.equals("Ja") && !wiederholen.equals("ja") && !wiederholen.equals("JA")
					&& !wiederholen.equals("Nein") && !wiederholen.equals("nein") && !wiederholen.equals("NEIN"));

		} while (wiederholen.equals("Ja") || wiederholen.equals("ja") || wiederholen.equals("JA"));

	}

	public static double farkartenBestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		int anzahlTicket = 0;

		while (anzahlTicket < 1 || anzahlTicket > 10) {

			System.out.print("Anzahl der Tickets (1 - 10 Möglich): ");
			anzahlTicket = tastatur.nextInt();

			if (anzahlTicket < 1 || anzahlTicket > 10) {

				System.out.println("Sie können nur 1 bis 10 Tickets kaufen");
				System.out.println("Bitte geben Sie eine neue Anzahl ein \n");
			}
		}

		double zuZahlenderBetrag = 0;
		int Fahrkartenart;

		String[] FahrkartenBezeichnung = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };

		double[] FahrkartenPreise = { 2.90, 3.30, 3.60, 1.90, 8.60, 8.60, 9.60, 23.50, 24.30, 24.90 };

		System.out.println("Wählen Sie ein Ticket aus! \n");

		int länge = 9;

		if (FahrkartenBezeichnung.length < 10) {

			länge = FahrkartenBezeichnung.length;
		}

		for (int i = 0; i < länge; i++) {
			System.out.printf(" (" + (i + 1) + ") " + FahrkartenBezeichnung[i] + " [%.2f €", FahrkartenPreise[i]);
			System.out.print("] \n");
		}

		if (FahrkartenBezeichnung.length >= 10) {

			for (int i = 9; i < FahrkartenBezeichnung.length; i++) {
				System.out.printf("(" + (i + 1) + ") " + FahrkartenBezeichnung[i] + " [%.2f €", FahrkartenPreise[i]);
				System.out.print("] \n");
			}

		}

		do {
			Fahrkartenart = tastatur.nextInt();

			if (Fahrkartenart > FahrkartenBezeichnung.length || Fahrkartenart < 1) {
				System.out.println("Ungültige Eingabe!");
				System.out.println("Geben Sie eine neue Zahl ein:");
			}

		} while (Fahrkartenart > FahrkartenBezeichnung.length || Fahrkartenart < 1);

		zuZahlenderBetrag = FahrkartenPreise[Fahrkartenart - 1];

		zuZahlenderBetrag = zuZahlenderBetrag * anzahlTicket;
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;

		String Kartenzahlung;

		System.out.printf("Zu zahlen: %.2f €\n", zuZahlenderBetrag);

		System.out.println("\nMöchten Sie Bar oder mit Karte zahlen: [Bar][Karte]");

		do {

			Kartenzahlung = tastatur.next();

			if (!Kartenzahlung.equals("Bar") & !Kartenzahlung.equals("bar") & !Kartenzahlung.equals("BAR")
					& !Kartenzahlung.equals("Karte") & !Kartenzahlung.equals("karte")
					& !Kartenzahlung.equals("KARTE")) {
				System.out.println("Ungültige Eingabe - Geben Sie nur [Bar][Karte] ein");
			}

		} while (!Kartenzahlung.equals("Bar") & !Kartenzahlung.equals("bar") & !Kartenzahlung.equals("BAR")
				& !Kartenzahlung.equals("Karte") & !Kartenzahlung.equals("karte") & !Kartenzahlung.equals("KARTE"));

		if (Kartenzahlung.equals("Karte") || Kartenzahlung.equals("karte") || Kartenzahlung.equals("KARTE")) {

			System.out.println("Halten Sie Ihre Karte ans Lesegerät:");

			for (int i = 0; i < 1; i++) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			for (int i = 0; i < 4; i++) {
				System.out.print("-");
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			System.out.println("\nZahlung wird verarbeitet");

			for (int i = 0; i < 8; i++) {
				System.out.print("-");
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			System.out.println("\nZahlung erfolgreich");

			for (int i = 0; i < 1; i++) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			eingezahlterGesamtbetrag = zuZahlenderBetrag;

		} else {

			System.out.println("Erlaubte Münzen/Scheine:");
			System.out.println("  *****      *****      *****      *****      *****      *****      *****      *****");
			System.out
					.println("*       *  *       *  *       *  *       *  *       *  *       *  *       *  *       *");
			System.out
					.println("*   1   *  *   2   *  *   5   *  *  10   *     20   *  *  50   *  *   1   *  *   2   *");
			System.out
					.println("*  cent *  *  cent *  *  cent *  *  cent *  *  cent *  *  cent *  *  Euro *  *  Euro *");
			System.out.println("  *****      *****      *****      *****      *****      *****      *****      *****");
			System.out.println("");
			System.out.println(
					" ---------------   ---------------   ---------------   ---------------   ---------------   ---------------");
			System.out.println(
					"|               | |               | |               | |               | |               | |               |");
			System.out.println(
					"|    5 Euro     | |    10 Euro    | |    20 Euro    | |    50 Euro    | |    100 Euro   | |    200 Euro   |");
			System.out.println(
					"|               | |               | |               | |               | |               | |               |");
			System.out.println(
					" ---------------   ---------------   ---------------   ---------------   ---------------   ---------------");

			while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
				double ausstehendeBetrage = zuZahlenderBetrag;
				ausstehendeBetrage = ausstehendeBetrage - eingezahlterGesamtbetrag;
				System.out.printf("Noch zu zahlen: %.2f €\n", ausstehendeBetrage);

				double eingeworfeneMünze;

				do {

					eingeworfeneMünze = tastatur.nextDouble();

					if (eingeworfeneMünze != 0.01 & eingeworfeneMünze != 0.02 & eingeworfeneMünze != 0.05
							& eingeworfeneMünze != 0.10 & eingeworfeneMünze != 0.20 & eingeworfeneMünze != 0.50
							& eingeworfeneMünze != 1 & eingeworfeneMünze != 2 & eingeworfeneMünze != 5
							& eingeworfeneMünze != 10 & eingeworfeneMünze != 20 & eingeworfeneMünze != 50
							& eingeworfeneMünze != 100 & eingeworfeneMünze != 200) {

						System.out.print("Ungültige Eingabe (mind. 5Ct, höchstens 200 Euro):");
					}

				} while (eingeworfeneMünze != 0.01 & eingeworfeneMünze != 0.02 & eingeworfeneMünze != 0.05
						& eingeworfeneMünze != 0.10 & eingeworfeneMünze != 0.20 & eingeworfeneMünze != 0.50
						& eingeworfeneMünze != 1 & eingeworfeneMünze != 2 & eingeworfeneMünze != 5
						& eingeworfeneMünze != 10 & eingeworfeneMünze != 20 & eingeworfeneMünze != 50
						& eingeworfeneMünze != 100 & eingeworfeneMünze != 200);

				eingezahlterGesamtbetrag += eingeworfeneMünze;

			}

		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {

			System.out.printf("Der Rückgabebetrag wird ausgezahlt: %.2f €\n", rückgabebetrag);

			rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 200.0) // 2 EURO-Münzen
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    200 Euro   |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				rückgabebetrag -= 200.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 100.0) // 2 EURO-Münzen
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    100 Euro   |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				rückgabebetrag -= 100.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 50.0) // 2 EURO-Münzen
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    50 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				rückgabebetrag -= 50.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 20.0) // 2 EURO-Münzen
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    20 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				rückgabebetrag -= 20.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 10.0) // 2 EURO-Münzen
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    10 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				rückgabebetrag -= 10.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 5.0) // 2 EURO-Münzen
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    5 Euro     |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				rückgabebetrag -= 5.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*   2   *");
				System.out.println("*  Euro *");
				System.out.println("  *****");
				rückgabebetrag -= 2.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*   1   *");
				System.out.println("*  Euro *");
				System.out.println("  *****");
				rückgabebetrag -= 1.0;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*  50   *");
				System.out.println("*  cent *");
				System.out.println("  *****");
				rückgabebetrag -= 0.5;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*  20   *");
				System.out.println("*  cent *");
				System.out.println("  *****");
				rückgabebetrag -= 0.2;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*  10   *");
				System.out.println("*  cent *");
				System.out.println("  *****");
				rückgabebetrag -= 0.1;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*   5   *");
				System.out.println("*  cent *");
				System.out.println("  *****");
				rückgabebetrag -= 0.05;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.02)// 2 CENT-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*   2   *");
				System.out.println("*  cent *");
				System.out.println("  *****");
				rückgabebetrag -= 0.02;
			}
			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.01)// 1 CENT-Münzen
			{
				System.out.println("  *****");
				System.out.println("*       *");
				System.out.println("*   1   *");
				System.out.println("*  cent *");
				System.out.println("  *****");
				rückgabebetrag -= 0.01;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

}